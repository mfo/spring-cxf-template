package my.cxf.test;

import javax.jws.WebService;

@WebService
public interface EchoService {

	public String echo(String text);

}
