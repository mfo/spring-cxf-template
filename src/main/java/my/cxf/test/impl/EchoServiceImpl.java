package my.cxf.test.impl;

import javax.jws.WebService;

import my.cxf.test.EchoService;

@WebService(serviceName = "EchoService")
public class EchoServiceImpl  implements EchoService {

	@Override
	public String echo(String text) {
		return text.toUpperCase();
	}
	
	public String noWebMethod(){
		return null;
	}
}
